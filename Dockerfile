FROM node:8.4.0

RUN mkdir -p /usr/src/api-app
WORKDIR /usr/src/api-app

COPY package.json /usr/src/api-app/
RUN npm install

COPY . /usr/src/api-app
EXPOSE 4000
EXPOSE 27017
# EXPOSE 2881

CMD ["node","./app.js","&"]

# CMD ["node","./socket_app.js" ,"&"]

