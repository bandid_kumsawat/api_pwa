var mongoose = require('mongoose');

// var SchemaTypes = mongoose.Schema.Types;

var SensorSchema = new mongoose.Schema({

    device_id: String,
    device_name: String,
    createAt: String,
    updateAt: String,
    station_name: String,
    detail: String

});
mongoose.model('Device', SensorSchema);