var mongoose = require('mongoose');

// var SchemaTypes = mongoose.Schema.Types;

mongoose.model('filter', new mongoose.Schema({

    topic: String,
    collect: Boolean,
    branch_code: String,
    period: Number,
    duplicate: Boolean
    
},{collection: 'topic_info'}));