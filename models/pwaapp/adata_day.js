var mongoose = require('mongoose');

// var SchemaTypes = mongoose.Schema.Types;

mongoose.model('adata_day', new mongoose.Schema({

        "branch_code": String,
        "station_code": String,
        "station_type": String,
        "station_name": Number,
        "device_code": String,
        "name": String,
        "code": String,
        "date_start": Date,
        "date_end": Date,
        "min": Number,
        "max": Number,
        "avg": Number,
        "consumption": Number,
        "value": Array

},{collection: 'adata_day'}));