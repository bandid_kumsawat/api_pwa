var mongoose = require('mongoose');

// var SchemaTypes = mongoose.Schema.Types;

var SensorSchema = new mongoose.Schema({

    branch_id: String,
    branch_id_data_timestamp: String,
    data: Object,
    createAt: String
});
mongoose.model('scada', SensorSchema);