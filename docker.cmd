# for api
docker build -t pwa_api_app/node-express .
docker run --name apiapp -p 14000:4000 pwa_api_app/node-express


docker run --name apiapp -p 14000:4000 -p 27017:27017 pwa_api_app/node-express

# for socket
docker build -t pwa_socker_app/node-socker .
docker run --name sockapp -p 12181:2181 pwa_socker_app/node-socker


# kill all process exited
docker ps -a | grep Exit | cut -d ' ' -f 1 | xargs sudo docker rm
# view process
docker ps -a

# remove container
docker image rm -f [name]


# main run

docker run --name apiapp -p 14000:4000 -p 12181:2181 pwa_api_app/node-express


# in HQ
docker run --name apiapp -p 4000:4000 -p 2181:2181 pwa_api_app/node-express


# tranfer file
scp api_pwa.tar root@wellmonitors.site:/root

# logging 
docker logs --tail 50 --follow --timestamps mediawiki_web_1

# save image
docker save --output hello-world.tar {your image name or ID}

# load image
docker load --input hello-world.tar