var router = require('express').Router();

router.use('/', require('./users'));
router.use('/sensor', require('./sensors'));
router.use('/station', require('./stations'));
// router.use('/profiles', require('./profiles'));
// router.use('/articles', require('./articles'));
// router.use('/tags', require('./tags'));


// router for device app
router.use("/device", require("./devices"))
router.use('/scadasync', require('./scadasyncs'));

router.use('/aggregate', require('./aggregate'))
router.use('/filter', require('./filter'))

router.use(function(err, req, res, next){
  if(err.name === 'ValidationError'){
    return res.status(422).json({
      errors: Object.keys(err.errors).reduce(function(errors, key){
        errors[key] = err.errors[key].message;

        return errors;
      }, {})
    });
  }

  return next(err);
});

module.exports = router;