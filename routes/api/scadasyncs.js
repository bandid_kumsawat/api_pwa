var router = require('express').Router();
var mongoose = require('mongoose');
var SCADA = mongoose.model('scada');

router.post('/find:branch',function(req, res, next){
    var qry = {};
    qry.branch_id = req.params.branch.slice(1, req.params.branch.length);
    console.log(qry)
    Promise.all([      
        SCADA.find(qry)
    ]).then(function(result){
        var out = {}
        out.item = result[0];
        return res.json(out)        
    }).catch(next)
})

router.post('/find/all',function(req, res, next){
    var qry = {};
    Promise.all([      
        SCADA.find(qry)
    ]).then(function(result){
        var out = {}
        out.item = result[0];
        return res.json(out)        
    }).catch(next)
})

router.post('/save', function(req, res, next) {
    var SCADA_new = new SCADA();

    if(typeof req.body.branch_id !== 'undefined'){
        SCADA_new.branch_id = req.body.branch_id
    }
    if(typeof  req.body.data !== 'undefined'){
        SCADA_new.data = req.body.data
    }
    if(typeof req.body.branch_id_data_timestamp !== 'undefined'){
        SCADA_new.branch_id_data_timestamp = req.body.branch_id_data_timestamp
    }
    SCADA_new.createAt = new Date()

    SCADA_new.save().then(function(){
        return res.json({
            mes: "Seccess"
        });
    }).catch(next);

});

module.exports = router;