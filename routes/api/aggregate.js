var router = require('express').Router();
var mongoose = require('mongoose');
var Aggregation_ = mongoose.model('adata');
var Aggregation_day = mongoose.model('adata_day');
var Aggregation_month = mongoose.model('adata_month');
var Aggregation_year = mongoose.model('adata_year');
var province = mongoose.model('province');
var metadata = mongoose.model('metadata')
var _ = require('underscore')
// check token
var auth = require('../auth');

// query find province 
router.post('/province/find', (req, res, next) => {
    Promise.all([
        province.find({index: 0}),
    ]).then(function(result){
        return res.json(result[0]);
    }).catch(next);
})

// query update province
router.put('/province/update', (req, res, next) => {
    Promise.all([
        province.findOne({index: req.body.index}),
    ]).then(function(result){
        var tmp_msg = result[0]
        tmp_msg.detail = req.body.detail

        tmp_msg.save().then(function(err){
            console.log(err)
            return res.json(tmp_msg);
        })  
    }).catch(next);
})

router.post('/find/all' , auth.required, function(req, res, next){

    if ((req.payload.exp - req.payload.iat) > 0){
        var qry = {};
        Promise.all([      
            Aggregation_.find(),
        ]).then(function(result){
            var out = {}
            out.Aggregation_ = result[0];
            out.jwt = {
                exp: 1,
                detail: "OK."
            }
            return res.json(out)        
        }).catch(next);
    }else{
        var out = {};
        out.jwt = {
            exp: 0,
            detail: "Your JWT is expire"
        }
        return res.json(out)        
    }
})

// query field 1 [ branch_code ]
// query field 1 [ branch_code ]
router.post("/field_branch", (req, res, next) => {
    var qry = {};
    Promise.all([      
        Aggregation_.find(qry).distinct("branch_code"),
    ]).then(function(result){
        var qry = {
            name: {
                $in: result[0]
            }
        };
        Promise.all([      
            metadata.find(qry)
        ]).then(function(result){
            var result = result[0]
            var new_arr = []
            result.forEach((item, index, arr) => {
                new_arr.push({
                    'ww': item.name,
                    'ww_desc': item.description
                })
            })
            var out = {}
            out.branch = {
                ww_all: new_arr
            }
            out.jwt = {
                exp: 1,
                detail: "OK."
            }
            return res.json(out)        
        }).catch(next);      
    }).catch(next);
})
// router.post("/field_branch", (req, res, next) => {
//     var qry = {};
//     Promise.all([      
//         Aggregation_.find(qry).distinct("branch_code"),
//     ]).then(function(result){
//         var out = {}
//         out.branch_code = result[0]
//         return res.json(out)        
//     }).catch(next);
// })

// query field 2 [ station_code ] 
router.post("/field_station", (req, res, next) => {
    var qry = {
        branch_code: req.body.branch_code
    };
    Promise.all([      
        Aggregation_.find(qry).distinct("station_code"),
    ]).then(function(result){
        var out = {}
        out.station_code = result[0]
        return res.json(out)        
    }).catch(next);
})

// query field 3 [ station_type ]
router.post("/field_type", (req, res, next) => {
    var qry = {
        branch_code: req.body.branch_code,
        station_code: req.body.station_code
    };
    Promise.all([      
        Aggregation_.find(qry).distinct("station_type"),
    ]).then(function(result){
        var out = {}
        out.station_type = result[0]
        return res.json(out)        
    }).catch(next);
})

// query field 4 [ station_name ] ถ้าสถานีมี type หลายสถานี
router.post("/field_type_sub", (req, res, next) => {
    var qry = {
        branch_code: req.body.branch_code,
        station_code: req.body.station_code,
        station_type: req.body.station_type
    };
    Promise.all([      
        Aggregation_.find(qry).distinct("station_name"),
    ]).then(function(result){
        var out = {}
        out.station_name = result[0]
        return res.json(out)        
    }).catch(next);
})

// query field 5 [ device_code ]
router.post("/field_deviccode", (req, res, next) => {
    var qry = {
        branch_code: req.body.branch_code,
        station_code: req.body.station_code,
        station_type: req.body.station_type,
        station_name: req.body.station_name
    };
    Promise.all([      
        Aggregation_.find(qry).distinct("device_code"),
    ]).then(function(result){
        var out = {}
        out.device_code = result[0]
        return res.json(out)        
    }).catch(next);
})

// query field 6 [ name ]
router.post("/field_devicname", (req, res, next) => {
    var qry = {
        branch_code: req.body.branch_code,
        station_code: req.body.station_code,
        station_type: req.body.station_type,
        station_name: req.body.station_name,
        device_code: req.body.device_code
    };
    Promise.all([      
        Aggregation_.find(qry).distinct("name"),
    ]).then(function(result){
        var out = {}
        out.name = result[0]
        return res.json(out)        
    }).catch(next);
})

// query field 7 [ code ]
router.post("/field_param", (req, res, next) => {
    var qry = {
        branch_code: req.body.branch_code,
        station_code: req.body.station_code,
        station_type: req.body.station_type,
        station_name: req.body.station_name,
        device_code: req.body.device_code,
        name: req.body.name
    };
    Promise.all([      
        Aggregation_.find(qry).distinct("code"),
    ]).then(function(result){
        var out = {}
        out.code = result[0]
        return res.json(out)        
    }).catch(next);
})

// router.post('/fields', (req, res, next) => {
//     var fields = []
//     Promise.all([
//         Aggregation_.find({}).distinct("branch_code"),
//     ]).then((result_branch_code) => {
//         result_branch_code[0].forEach((item_branch_code,index_branch_code) => {
//             fields[index_branch_code].branch_code = item_branch_code
//             Promise.all([
//                 Aggregation_.find({
//                     branch_code: item_branch_code
//                 }).distinct("station_code"),
//             ]).then((result_station_code) => {
//                 result_station_code[0].forEach((item_station_code, index_station_code) => {
//                     fields[index_branch_code].station_code = item_station_code
//                 })
//             })
//         })
//         return res.json()  
//     })
// })

router.post('/find' , auth.required, function(req, res, next){
    if ((req.payload.exp - req.payload.iat) > 0){

        var st_time = new Date(req.body.start_time) //.toISOString().slice(0, -1)
        var ed_time = new Date(req.body.end_time)   //.toISOString().slice(0, -1)
        var qry = {
            branch_code: String(req.body.branch_code),
            station_code: req.body.station_code,
            station_type: req.body.station_type,
            station_name: req.body.station_name,
            device_code: req.body.device_code,
            name: req.body.name,
            code: req.body.code,
            date_start:{
                $gt: st_time,
                $lt: ed_time
            }
        };
        Promise.all([      
            Aggregation_.find(qry).sort({date_start: -1}),
            // {branch_code:"5511011"station_code:"CK"station_type:"DPB"station_name:0device_code:"POWER"name:"1"code:"VOLTL1L3",date_start:{$gt:new ISODate('2020-04-06T21:00:00.000+00:00'),$lt:new ISODate('2020-04-06T21:15:00.000+00:00')}}
            // {branch_code:"5511011",station_code:"CK",station_type:"DPB",station_name:0,device_code:"POWER",name:"1",code:"VOLTL1L3",date_start:{$gt:new ISODate('2020-04-06T21:00:00.000+00:00'),$lt:new ISODate('2020-04-06T21:15:00.000+00:00')}}
            //         2020-04-06T21:00:00.000+00:000
            //        2020-04-06T21:00:00.000+00:00              2020-04-06T21:15:00.000+00:00
            //   {"branch_code":5511012,"station_code":"CK","station_type":"DPB","station_name":0,"device_code":"POWER","name":"1","code":"VOLTL2L3","start_time":"2020-04-06T21:00:00.000+00:00","end_time":"2020-04-06T21:15:00.000+00:00"}
        ]).then(function(result){
            result[0].forEach((item, index, arr) => {
                result[0][index].value = _.sortBy(item.value, 'date')
            })
            var out = {}
            out.Aggregation_ = result[0]
            out.jwt = {
                exp: 1,
                detail: "OK."
            }
            return res.json(out)        
        }).catch(next);
    }else{
        var out = {};
        out.jwt = {
            exp: 0,
            detail: "Your JWT is expire"
        }
        return res.json(out)        
    }
})

router.post('/find/day' , auth.required, function(req, res, next){
    if ((req.payload.exp - req.payload.iat) > 0){
        var st_time = new Date(req.body.start_time) //.toISOString().slice(0, -1)
        var ed_time = new Date(req.body.end_time)   //.toISOString().slice(0, -1)
        var qry = {
            branch_code: String(req.body.branch_code),
            station_code: req.body.station_code,
            station_type: req.body.station_type,
            station_name: req.body.station_name,
            device_code: req.body.device_code,
            name: req.body.name,
            code: req.body.code,
            date_start:{
                $gt: st_time,
                $lt: ed_time
            }
        };
        Promise.all([      
            Aggregation_day.find(qry).sort({date_start: -1}),
        ]).then(function(result){
            var out = {}
            out.Aggregation_day = result[0]
            out.jwt = {
                exp: 1,
                detail: "OK."
            }
            return res.json(out)        
        }).catch(next);
    }else{
        var out = {};
        out.jwt = {
            exp: 0,
            detail: "Your JWT is expire"
        }
        return res.json(out)        
    }
})

router.post('/find/month' , auth.required, function(req, res, next){
    if ((req.payload.exp - req.payload.iat) > 0){
        var st_time = new Date(req.body.start_time) //.toISOString().slice(0, -1)
        var ed_time = new Date(req.body.end_time)   //.toISOString().slice(0, -1)
        var qry = {
            branch_code: String(req.body.branch_code),
            station_code: req.body.station_code,
            station_type: req.body.station_type,
            station_name: req.body.station_name,
            device_code: req.body.device_code,
            name: req.body.name,
            code: req.body.code,
            date_start:{
                $gt: st_time,
                $lt: ed_time
            }
        };
        Promise.all([      
            Aggregation_month.find(qry).sort({date_start: -1}),
        ]).then(function(result){
            var out = {}
            out.Aggregation_month = result[0]
            out.jwt = {
                exp: 1,
                detail: "OK."
            }
            return res.json(out)        
        }).catch(next);
    }else{
        var out = {};
        out.jwt = {
            exp: 0,
            detail: "Your JWT is expire"
        }
        return res.json(out)        
    }
})

router.post('/find/month' , auth.required, function(req, res, next){
    if ((req.payload.exp - req.payload.iat) > 0){
        var st_time = new Date(req.body.start_time) //.toISOString().slice(0, -1)
        var ed_time = new Date(req.body.end_time)   //.toISOString().slice(0, -1)
        var qry = {
            branch_code: String(req.body.branch_code),
            station_code: req.body.station_code,
            station_type: req.body.station_type,
            station_name: req.body.station_name,
            device_code: req.body.device_code,
            name: req.body.name,
            code: req.body.code,
            date_start:{
                $gt: st_time,
                $lt: ed_time
            }
        };
        Promise.all([      
            Aggregation_month.find(qry).sort({date_start: -1}),
        ]).then(function(result){
            var out = {}
            out.Aggregation_month = result[0]
            out.jwt = {
                exp: 1,
                detail: "OK."
            }
            return res.json(out)        
        }).catch(next);
    }else{
        var out = {};
        out.jwt = {
            exp: 0,
            detail: "Your JWT is expire"
        }
        return res.json(out)        
    }
})

router.post('/find/year' , auth.required, function(req, res, next){
    if ((req.payload.exp - req.payload.iat) > 0){
        var st_time = new Date(req.body.start_time) //.toISOString().slice(0, -1)
        var ed_time = new Date(req.body.end_time)   //.toISOString().slice(0, -1)
        var qry = {
            branch_code: String(req.body.branch_code),
            station_code: req.body.station_code,
            station_type: req.body.station_type,
            station_name: req.body.station_name,
            device_code: req.body.device_code,
            name: req.body.name,
            code: req.body.code,
            date_start:{
                $gt: st_time,
                $lt: ed_time
            }
        };
        Promise.all([      
            Aggregation_year.find(qry).sort({date_start: -1}),
        ]).then(function(result){
            var out = {}
            out.Aggregation_year = result[0]
            out.jwt = {
                exp: 1,
                detail: "OK."
            }
            return res.json(out)        
        }).catch(next);
    }else{
        var out = {};
        out.jwt = {
            exp: 0,
            detail: "Your JWT is expire"
        }
        return res.json(out)        
    }
})

router.post('/find/metadata' , auth.required, function(req, res, next){
    if ((req.payload.exp - req.payload.iat) > 0){
        var qry = {
            name: {
                $in: req.body
            }
        };
        Promise.all([      
            metadata.find(qry)
        ]).then(function(result){
            var result = result[0]
            var new_arr = []
            result.forEach((item, index, arr) => {
                new_arr.push({
                    'ww': item.name,
                    'ww_desc': item.description
                })
            })
            var out = {}
            out.metadata = [
                new_arr
            ]
            out.jwt = {
                exp: 1,
                detail: "OK."
            }
            return res.json(out)        
        }).catch(next);
    }else{
        var out = {};
        out.jwt = {
            exp: 0,
            detail: "Your JWT is expire"
        }
        return res.json(out)        
    }
})

module.exports = router;

// db.adata.aggregate(
//     [ { $group : { _id : "$device_code" } } ]
// )


// Aggregation_.distinct("station_code"),
// Aggregation_.distinct("station_type"),
// Aggregation_.distinct("station_name"),
// Aggregation_.distinct("device_code"),
// Aggregation_.distinct("name"),
// Aggregation_.distinct("code"),