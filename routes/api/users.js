var mongoose = require('mongoose');
var router = require('express').Router();
var passport = require('passport');
var User = mongoose.model('User');
var auth = require('../auth');

router.post('/users/find', function(req, res, next){
  User.find({type: "user"}).then(function (user){
    res.json(user);
  })
});

router.get('/user', auth.required, function(req, res, next){
  User.findById(req.payload.id).then(function(user){
    if(!user){ return res.sendStatus(401); }
    return res.json({user: user.toAuthJSON()});
  }).catch(next);
});

router.delete('/user', function(req, res, next){
  User.findOneAndRemove({_id: req.body.primary_key}).then(function(user){
    if(!user){ return res.sendStatus(401); }
    return res.json(user);
  });
});

router.put('/user', function(req, res, next){
  var qry
  // qry.username = req.body.user.username;
  qry = req.body.primary_key
  User.findById(qry).then(function(user){
    if(!user){ return res.sendStatus(401); }

    if(typeof req.body.user.type !== 'undefined'){
      user.type = req.body.user.type;
    }
    if(typeof req.body.user.username !== 'undefined'){ // and check username unique
      user.username = req.body.user.username;
    }
    if(typeof req.body.user.password !== 'undefined'){
      user.setPassword(req.body.user.password);
    }
    if(typeof req.body.user.email !== 'undefined'){
      user.email = req.body.user.email;
    }
    if(typeof req.body.user.mainsite !== 'undefined'){
      user.mainsite = req.body.user.mainsite;
    }
    return user.save().then(function(){
      return res.json({user: user.toAuthJSON(), status: true});
    });
  }).catch(next);
});

router.post('/users/login', function(req, res, next){

  if(!req.body.user.username){
    return res.status(422).json({errors: {username: "can't be blank"}});
  }

  if(!req.body.user.password){
    return res.status(422).json({errors: {password: "can't be blank"}});
  }

  passport.authenticate('local', {session: false}, function(err, user, info) {
    if(err){ return next(err); }

    if(user){
      user.token = user.generateJWT();
      return res.json({user: {data:1, auth: user.toAuthJSON()}});
    } else {
      // return res.status(422).json(info);
      return res.json({user: {data: 0}});
    }
  })(req, res, next);
});

router.post('/users', function(req, res, next){
  var user = new User();

  user.username = req.body.user.username;
  user.email = req.body.user.email;
  user.type = req.body.user.type;
  user.mainsite = req.body.user.mainsite;
  user.setPassword(req.body.user.password);
  user.save().then(function(){
    return res.json({user: user.toAuthJSON()});
  }).catch(next);
});

module.exports = router;
