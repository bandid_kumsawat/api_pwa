var MongoClient = require('mongodb').MongoClient;
var router = require('express').Router();
const { filter } = require('methods');
var mongoose = require('mongoose');
var topic_filter = mongoose.model('filter')

router.post("/findFilter", (req, res, next) => {
    var qry = {
        branch_code: req.body.branch_code
    };
    Promise.all([      
        topic_filter.find(qry)
    ]).then(function(result){
        var out = {}
        out.filter = result[0]
        return res.json(out)        
    }).catch(next);
})
// function getUsers(user) {
//     return new Promise(function (resolve, reject) {
//         var sql = "SELECT username, groups FROM users";
//         if (user && user.username) {
//             sql = "SELECT username, password, groups FROM users WHERE username = '" + user.username + "'";
//         }
//         db_usr.all(sql, function (err, rows) {
//             if (err) {
//                 reject(err);
//             } else {
//                 resolve(rows);
//             }
//         });   
//     });
// }
const updatemany_db = (qry, collect) => {
    return new Promise(function (resolve, reject) {
        MongoClient.connect('mongodb://127.0.0.1:27017/', function(err, db) {
            if (err) throw err;
            var dbo = db.db("pwa_scada");
            dbo.collection("gatewarfilter").updateMany(qry, {$set:{filter: collect}}, function(err, rows) {
                if (err) throw err;
                db.close();
                if (err) {
                    reject(err);
                } else {
                    resolve(rows);
                }
            });
        });
    })
}
const connect = () => {
    return new Promise((resolve, reject) => {
        MongoClient.connect('mongodb://localhost:27017/', function(err, db) {
            var dbo = db.db("pwa_scada");
            if (err){
                reject(err);
            }else {
                resolve(dbo)
            }
        })
    })
}
const query = (dbo, qry, collect) => {
    return new Promise((resolve, reject) => {
        dbo.collection("topic_info").updateMany(qry, {$set:{collect: collect}}, function(err, rows) {
            if (err) {
                reject(err);
            } else {
                resolve(rows);
            }
        });
    })
}
router.post('/save_all', (req, res, next) => {
    var qry = {
        branch_code: req.body.branch_code
    }
    connect().then((result1) => {
        query(result1 ,qry, req.body.collect).then((result2) => {
            res.json(result2)
        })
    })
    // var updated = query( ,qry, req.body.collect)
});


// router.post('/save_all', (req, res, next) => {
//     var qry = {
//         branch_code: req.body.branch_code
//     }
//     var update = updatemany_db(qry, req.body.collect);
//     update.then((result) => {
//         return res.json({
//             result: result
//         })
//     }).catch((err) => {
//         return res.json({
//             result: err
//         })
//     })
// })

router.put("/save", (req, res, next) => {
    var qry = {
        topic: req.body.topic,
        branch_code: req.body.branch_code
    };
    Promise.all([      
        topic_filter.findOne(qry).count(),
        topic_filter.findOne(qry)
    ]).then(function(result){
        if (result[0] == 1){
            var filter_result = result[1]
            filter_result.collect = req.body.filter
            filter_result.period = req.body.period
            filter_result.duplicate = req.body.duplicate
            filter_result.save().then(function(err){
                console.log(err)
                var ress = {
                    filter_result: filter_result,
                    status: true
                }
                return res.json(ress);
            }).catch(next)   
        }else {
            var ress = {
                status: false
            }
            return res.json(ress)
        }
    }).catch(next);
})


router.delete('/' ,function(req, res, next){

    var _id = req.body._id;
    var qry = {};
    qry._id = _id;
    console.log(qry)
    Promise.all([ 
        topic_filter.findOneAndRemove(qry)                               // result 0
    ]).then(function(result){
        return res.json(result)
    });

});

module.exports = router;