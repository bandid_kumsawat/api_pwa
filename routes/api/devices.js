var router = require('express').Router();
var mongoose = require('mongoose');
var Device = mongoose.model('Device');

// check token
var auth = require('../auth');

router.post('/find/all' , auth.required, function(req, res, next){
    // console.log("============== payload jwt ==============")
    // console.log(req.payload)
    // console.log("=========================================")
    // console.log("============== exp - iat ==============")
    // console.log(req.payload.exp - req.payload.iat);       // 'exp' age 3 mounth
    // console.log("=======================================")
    if ((req.payload.exp - req.payload.iat) > 0){
        var qry = {};
        Promise.all([      
            Device.find(),
        ]).then(function(result){
            var out = {}
            out.device = result[0];
            out.jwt = {
                exp: 1,
                detail: "OK."
            }
            return res.json(out)        
        }).catch(next);
    }else{
        var out = {};
        out.jwt = {
            exp: 0,
            detail: "Your JWT is expire"
        }
        return res.json(out)        
    }
})

router.post('/update', auth.required, function(req, res, next) {
    var qry = { 
        device_id: (req.body.device_id !== 'undefined' ? req.body.device_id : "")
    };
    Promise.all([
        Device.find(),
    ]).then(function(result){
        var out = {}
        out.device = result[0];
        out.jwt = {
            exp: 1,
            detail: "OK."
        }
        return res.json(out)        
    }).catch(next);
});

router.post('/save', auth.required, function(req, res, next) {
    var qry = {}
    qry.device_id = req.body.device_id
    Promise.all([
        Device.find(qry).count()
    ]).then(function(result){
        if (result[0] !== 1){
            if ((req.payload.exp - req.payload.iat) > 0){
                var Device_new = new Device();
        
                Device_new.device_id = req.body.device_id
                Device_new.device_name = req.body.device_name
                Device_new.createAt = new Date()
                Device_new.station_name = req.body.station_name
                Device_new.detail = req.body.detail
                Device_new.updateAt = ""
        
                Device_new.save().then(function(){
        
                    var out = {}
                    out.jwt = {
                        exp: 1,
                        detail: "Your JWT is expire"
                    }
                    out.mes = "Success."
                    out.status = true;
                    return res.json(out);
                }).catch(next);
            } else {
                var out = {}
                out.jwt = {
                    exp: 1,
                    detail: "OK."
                }
                out.status = true;
                out.mes = "Success."
                return res.json(out);
            }
        } else {
            var out = {}
            out.mes = "Available device in database."
            out.status = false
            return res.json(out)
        }
    }).catch(next);

});

router.put('/', auth.required, function(req, res, next) {
    if ((req.payload.exp - req.payload.iat) < 0){
        return res.json({
            mes: "Please authentication.",
            status_select: false
        })
    } else {
        var device_id = req.body.primary_key;
        var qry = {};
        qry.device_id = device_id;
        Promise.all([ 
            Device.findOne(qry),
            Device.find(qry).count()
        ]).then(function(result){
            if (result[1] === 1){
                if(typeof req.body.update.device_id !== 'undefined'){
                    result[0].device_id = req.body.update.device_id;
                }
                if(typeof req.body.update.device_name !== 'undefined'){
                    result[0].device_name = req.body.update.device_name;
                }
                if(typeof req.body.update.detail !== 'undefined'){
                    result[0].detail = req.body.update.detail;
                }
                if(typeof req.body.update.station_name !== 'undefined'){
                    result[0].station_name = req.body.update.station_name; 
                }
                
                result[0].updateAt = new Date()

                result[0].save().then(function(err){
                    console.log(err)
                    return res.json({
                        data: result[0],
                        mes: 'updated station.',
                        status_insert: true
                    });
                }) 
            } else {
                var out = {};
                out.status = false;
                out.mes = "Unavailable device in database."
                return res.json(out);
            }

        });
    }
})


router.delete('/', auth.required ,function(req, res, next){
    if ((req.payload.exp - req.payload.iat) < 0){
        return res.json({
            mes: "Please authentication.",
            status_select: false
        })
    } else {
        device_id = req.body.primary_key;
        var qry = {};
        qry.device_id = device_id;
        Promise.all([ 
            Device.findOneAndRemove(qry)                               // result 0
        ]).then(function(result){
            // console.log(result[0])
            if (result[0] !== null){
                return res.json({
                    mes: "station id " + result[0].device_id + " deleted.",
                    result: result,
                    status: true
                })
            } else {
                return res.json({
                    mes: 'Device id "' + device_id + '" unavailable. Please insert device',
                    result: result,
                    status: false
                })
            }
        });
    }

});


module.exports = router;



// router.delete('/', auth.required, function(req, res, next){

//     if ((req.payload.exp - req.payload.iat) < 0){
//         return res.json({
//             mes: "Please authentication.",
//             status_select: false
//         })
//     } else {
//         device_id = req.body.primary_key;
//         var qry = {};
//         qry.device_id = device_id;
//         console.log(qry)
//         Promise.all([  
//             Device.find(qry).count() // result 0
//         ]).then(function(result){
//             // console.log(result[0])
//             if (result[0] === 1){
//                 Device.findOneAndRemove(qry)
//                 return res.json({
//                     mes: "station id " + result[0].device_id + " deleted.",
//                     result: result,
//                     status: true
//                 })
//             } else {
//                 console.log(result)
//                 return res.json({
//                     mes: 'station id "' + station_id + '" unavailable. Please insert device',
//                     result: result,
//                     status: false
//                 })
//             }
//         });
//     }

// });