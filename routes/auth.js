var jwt = require('express-jwt');
var secret = require('../config').secret;

function getTokenFromHeader(req){
  if (req.headers.authorization && req.headers.authorization.split(' ')[0] === 'Token' ||
      req.headers.authorization && req.headers.authorization.split(' ')[0] === 'Bearer') {
    return req.headers.authorization.split(' ')[1];
  }

  return null;
}

function getTokenFromRawHeaderApp(req){
  for (var i = 0;i < req.rawHeaders.length;i++){
    if (req.rawHeaders[i] === "Authorization"){
      if (req.rawHeaders[i + 1].split(' ')[0] === "Bearer"){
        return req.rawHeaders[i + 1].split(' ')[1];
      }
    }
  }
  return null;
}
console.log(secret)
var auth = {
  requiredAuthBearer: jwt({
    secret: secret,
    userProperty: 'payload',
    getToken: getTokenFromRawHeaderApp
  }),
  required: jwt({
    secret: secret,
    userProperty: 'payload',
    getToken: getTokenFromHeader
  }),
  optional: jwt({
    secret: secret,
    userProperty: 'payload',
    credentialsRequired: false,
    getToken: getTokenFromHeader
  })
};

module.exports = auth;
