var http = require('http'),
    path = require('path'),
    methods = require('methods'),
    express = require('express'),
    bodyParser = require('body-parser'),
    session = require('express-session'),
    cors = require('cors'),
    passport = require('passport'),
    errorhandler = require('errorhandler'),
    mongoose = require('mongoose'),
    moment = require('moment'),
    mqtt = require('mqtt');

var isProduction = process.env.NODE_ENV === 'production';

// Create global app object
var app = express();

app.use(cors());

// Normal express config defaults
app.use(require('morgan')('dev'));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use(require('method-override')());
app.use(express.static(__dirname + '/public'));

app.use(session({ secret: 'conduit', cookie: { maxAge: 60000 }, resave: false, saveUninitialized: false  }));

if (!isProduction) {
  app.use(errorhandler());
}

// if(isProduction){
//   mongoose.connect(process.env.MONGODB_URI);
// } else {
//   mongoose.connect('mongodb://localhost/PTT_PUMP');
//   mongoose.set('debug', true);
// }


// mongoose.connect('mongodb://dwdev.info:27117/pwa_scada');
mongoose.connect('mongodb://127.0.0.1:27017/pwa_scada');

require('./models/User');
require('./models/Sensor');
require('./models/Station');
require('./config/passport');


require('./models/pwaapp/adata')
require('./models/pwaapp/adata_day')
require('./models/pwaapp/adata_month')
require('./models/pwaapp/adata_year')
require('./models/pwaapp/metadata')
require('./models/pwaapp/province')
require("./models/pwaapp/Device")
require('./models/pwaapp/scada');
require('./models/pwaapp/filter');

app.use(require('./routes'));

/// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

/// error handlers

// development error handler
// will print stacktrace
if (!isProduction) {
  app.use(function(err, req, res, next) {
    console.log(err.stack);

    res.status(err.status || 500);

    res.json({'errors': {
      message: err.message,
      error: err
    }});
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.json({'errors': {
    message: err.message,
    error: {}
  }});
});

// finally, let's start our server...
var server = app.listen( process.env.PORT || 4000, function(){
  console.log('Listening on port ' + server.address().port);
});

const io = require("socket.io")(server, {
  handlePreflightRequest: (req, res) => {
      const headers = {
          "Access-Control-Allow-Headers": "Content-Type, Authorization",
          "Access-Control-Allow-Origin": req.headers.origin, //or the specific origin you want to give access to,
          "Access-Control-Allow-Credentials": true
      };
      res.writeHead(200, headers);
      res.end();
  }
});

var users = 0

//listen on every connection
io.on('connection', (socket) => {
console.log("isConnect");
  var clientMQTT = mqtt.connect("mqtt://iotdma.info",{
    port: 1883,
    host: 'mqtt://iotdma.info',
    clientId: 'MQTTJS____WEB____client_IN__SERVER_' + Math.random().toString(16).substr(2, 8),
    username: 'admin',
    password: '2020iotdmamqtt',
    keepalive: 60,
    reconnectPeriod: 1000,
    protocolId: 'MQIsdp',
    protocolVersion: 3,
    clean: true,
    encoding: 'utf8'
  })
  var topic_list=["SCADA/#"];// SCADA/REG1/5531011/KS/DPB/1/FLOW
  //var topic_list=["SCADA/#"];// SCADA/REG1/5531011/KS/DPB/1/FLOW
  clientMQTT.subscribe(topic_list,{qos:2});
  socket_con = socket
  users++
	console.log('New user connected ' + (users))

  socket.on('disconnect', (socket) => {
    clientMQTT.end();
    console.log("disconnect")
    console.log(socket)
    users--
  });


  //handle incoming messages
  clientMQTT.on('message',function(topic, message, packet){
    console.log("message is "+ message);
    console.log("topic is "+ topic);
    let json = JSON.stringify(message);
    let bufferOriginal = Buffer.from(JSON.parse(json).data);
    var res = [
      {
        tag: topic,
        val: bufferOriginal.toString('utf8')
      },{
        tag: 'datetime',
        val: moment(new Date()).format('MMMM DD YYYY, HH:mm:ss')
      }
    ]
    // console.log("send to client id : " + socket.id)
    // console.log(res)
    try {
      io.sockets.connected[socket.id].emit('sensor', res);
    }catch (err){
      console.log(err)
    }
    // io.sockets.broadcast.emit('sensor', res);
    // socket_con.broadcast.emit('sensor', res)
  });



  //listen on new_message
  // socket.on('sensor_value', (data) => {
  //     //broadcast the new message
  //     // console.log(data);
  //     // console.log(socket.id)

  //     socketid = socket.id

  //     // var res = [
  //     //   {
  //     //     tag: 'SCADA/REG1/5531011/BP1/DPB/1/FLOW_TOTAL',
  //     //     val: (Math.random(0,1) * 100).toFixed(2)
  //     //   },{
  //     //     tag: 'datetime',
  //     //     val: moment(new Date()).format('MMMM DD YYYY, HH:mm:ss')
  //     //   }, {
  //     //     tag: 'SCADA/REG1/5531011/BP1/DPB/1/PUMP/PBP1001/STATUS',
  //     //     val: 1
  //     //   }
  //     // ]
  //     // io.sockets.connected[socketid].emit('sensor', res);
  // })

  //listen on test
  // socket.on('test', (data) => {
  //   socket.broadcast.emit('test', {username : socket.username})
  //   socket_con.broadcast.emit('test', {username : socket.username})
  // })

})
